export class RenderTestClass {
  constructor(title: string, message: string) {
    this.title = title;
    this.message = message;
  }
  title: string;
  message: string;
}