import {RenderTestClass} from "../models/render_test_class";

export class HelloController {
    index(req, res) {
        res.send('hello.index');
    };
    renderTest(req, res) {
        const to = req.params.to;
        const render = new RenderTestClass("Modern JS", `Dear ${to} I shit my pants`);
        res.render('index', render);
    }
}

export default new HelloController();